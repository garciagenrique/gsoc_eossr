#!/usr/bin/env python

import requests

zenodo_url = "https://zenodo.org/API/records"


def query_zenodo(url=zenodo_url, **parameters):
    """
    Simple request.get() request to an API URL

    :param url: str
        API URL to call
    :return: requests.get() answer
    """
    response = requests.get(url, params=parameters)
    return response


def query_escape_community(url=zenodo_url):
    """
    Query the Records within the 'escape2020' community.

    :param url: str
        API URL to call
    :return: list
        List containing the Zenodo records of the `escape2020`
        community
    """
    params = {'communities': 'escape2020'}
    return query_zenodo(url, **params).json()['hits']['hits']
