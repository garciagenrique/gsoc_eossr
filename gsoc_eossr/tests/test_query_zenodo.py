#!/usr/bin/env python

def test_query_zenodo():
    from gsoc_eossr.zenodo import query_zenodo
    answer = query_zenodo()
    assert answer.status_code == 200


def test_query_escape_community():
    from gsoc_eossr.zenodo import query_escape_community
    answer = query_escape_community()
    assert isinstance(answer, list)
    assert len(answer) == 26
