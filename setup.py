#!/usr/bin/env python

from setuptools import find_packages, setup

setup(
    name='gsoc_eossr',
    version='v0.1',
    description="OSSR GSoC project to evaluate 2022 candidates",
    install_requires=[
        "requests>=2.25.0,<3.0",
        "pytest",
    ],
    packages=find_packages(),
    scripts=[],
    tests_require=['pytest'],
    author='Enrique Garcia',
    author_email='garcia@lapp.in2p3.fr',
    url='https://gitlab.in2p3.fr/garcia/gsoc_eossr',
    license='MIT',
)
